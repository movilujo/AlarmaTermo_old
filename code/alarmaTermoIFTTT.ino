#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <Time.h>

// Definimos parámetros
#define WIFISID "WifiID"
#define WIFIPASS "**********"

// Definimos los parámetros para el IFTTT
#define HOSTIFTTT "maker.ifttt.com"
#define EVENTO "AlarmaTermo"
#define IFTTTKEY "eHBy8Ml4dONNtnOInh-********_*************"

ESP8266WiFiMulti WiFiMulti;

// Utilizamos la conexión SSL del ESP8266
WiFiClientSecure client;

// Variable que permite ejecutar una sola vez la función
bool ejecutado = true;
// Intervalo en milisegundos entre envios de la alarma
long intervalo = 600000; 
long antAlarma = 0;

// Pines
const byte intTermo = 4;
const byte LedOK = 5;
const byte LedAlarm = 3;

// Variables interrupción de interruptor
volatile boolean isrAlarmaCentinela = false; // Detectar interrupción

void sendAlarm()
{
  // Cerramos cualquier conexión anterior
  if (client.connected())
  {
    client.stop();
  }

  // Esperamos hasta que se hayan enviado todos los datos
  client.flush();

  // Hacemos la petición mediante SSL
  if (client.connect(HOSTIFTTT, 443)) {
    // Construimos la petición HTTP
    String toSend = "GET /trigger/";
    toSend += EVENTO;
    toSend += "/with/key/";
    toSend += IFTTTKEY;
    toSend += " HTTP/1.1\r\n";
    toSend += "Host: ";
    toSend += HOSTIFTTT;
    toSend += "\r\n";
    toSend += "Connection: close\r\n\r\n";
    client.print(toSend);
  }

  // Esperamos hasta que se hayan enviado todos los datos
  client.flush();
  // Desconectamos del cliente
  client.stop();
}

void setup() {
 Serial.begin(115200);
 delay(500);

 pinMode(LedOK, OUTPUT);
 pinMode(LedAlarm, OUTPUT);

 digitalWrite(LedAlarm, HIGH);
 delay(500);
 digitalWrite(LedAlarm, LOW);
 delay(500);
 digitalWrite(LedAlarm, HIGH);

 // Conectamos a la red WiFi
 WiFiMulti.addAP(WIFISID, WIFIPASS);

 Serial.println();
 Serial.println();
 Serial.print("Eperando a conectar a la WiFi... ");

 while (WiFiMulti.run() != WL_CONNECTED) {
 Serial.print(".");
 delay(500);
 }

 Serial.println("");
 Serial.println("WiFi conectada");
 Serial.println("Direccion IP: ");
 Serial.println(WiFi.localIP());

 delay(500);

 // Configuración de interrupciones
 //pinMode(intTermo, INPUT_PULLUP); // Interrupción interruptor magnético
 pinMode(intTermo, INPUT); // Interrupción interruptor magnético
 attachInterrupt(intTermo, isrAlarma, FALLING); //Interrupción interruptor magnético

 digitalWrite(LedAlarm, LOW);
 digitalWrite(LedOK, HIGH);
 
}

void loop() {

  unsigned long currentMillis = millis();
  if (isrAlarmaCentinela && ejecutado)
  {
    sendAlarm();
    antAlarma = millis();
    ejecutado = false;
  }
  else if (isrAlarmaCentinela && !ejecutado)
  {
    
    if (currentMillis - antAlarma > intervalo)
    {
      ejecutado = true;
    }
  }
}

void isrAlarma() {
  // Marca ha ejecutado interrupción
  isrAlarmaCentinela = true;
  digitalWrite(LedAlarm, HIGH);
  digitalWrite(LedOK, LOW);
}
